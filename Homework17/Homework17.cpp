﻿#include <iostream>
#include <cmath>

class Animals {
public:
    Animals():dangerous(0), age(0), type("no type"), name("no name") {}
    Animals(short newDangerous, short newAge, std::string newType, std::string newName) :dangerous(newDangerous),
        age(newAge), type(newType), name(newName) {}
    short GetDangerous() { return dangerous; }
    short GetAge() { return age; }
    std::string GetType() { return type; }
    std::string GetName() { return name; }
    void ShowData() {
        std::cout << "Name: " << this->name << "\n" << "Type: " << this->type << "\n"
            << "Age: " << this->age << "\n" << "Dangerous: " << this->dangerous << "\n\n";
    }
private:
    short dangerous, age;
    std::string type, name;
};
class Vector {
public:
    Vector(float newX, float newY, float newZ): x(newX), y(newY), z(newZ){}
    float GetX() { return x; }
    float GetY() { return y; }
    float GetZ() { return z; }
    double Lenght() {
        return  std::sqrt(std::pow(x, 2) + std::pow(y, 2) + std::pow(z, 2));
    }
private:
    float x = static_cast<float>(0), y = static_cast<float>(0), z = static_cast<float>(0);
};

int main()
{
    Animals no_animals;
    no_animals.ShowData();
    Animals fox(13, 5, "Fox", "Alice");
    std::cout << "Name: " << fox.GetName() << "\n" << "Type: " << fox.GetType() << "\n"
        << "Age: " << fox.GetAge() << "\n" << "Dangerous: " << fox.GetDangerous() << "\n\n";
    Vector vector(2, -2, 1);
    std::cout << "Length vector: " << vector.Lenght() << std::endl;
 
}
